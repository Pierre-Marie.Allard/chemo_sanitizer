import chemosanitizer_functions
from chemosanitizer_functions import *




inchi = 'InChI=1/C10H11NO4/c1-15-10(13)9(11-14)6-7-2-4-8(12)5-3-7/h2-5,12,14H,6H2,1H3/b11-9+'
inchi = 'InChI=1S/C75H140O17P2/c1-5-9-13-17-19-21-23-25-27-29-31-37-45-53-72(77)85-61-70(91-74(79)55-47-38-32-30-28-26-24-22-20-18-14-10-6-2)63-89-93(81,82)87-59-69(76)60-88-94(83,84)90-64-71(92-75(80)56-48-40-34-36-44-52-68-58-66(68)50-42-16-12-8-4)62-86-73(78)54'


input_file_path = '/Users/pma/Dropbox/Research_UNIGE/git_repos/chemo_sanitizer/data/unique.tsv.gz'
myZip = gzip.open(input_file_path)

df = pd.read_csv(
	myZip,
	sep = '\t')


inchi_list = df['structureTranslated'].to_list() 

for inchi in inchi_list: 
    print(inchi)
    Chem.MolFromInchi(inchi)
    romol = Chem.MolFromInchi(inchi)
    if romol is not None:
        print(rdMolDescriptors.CalcNumUnspecifiedAtomStereoCenters(romol))
        print(rdMolDescriptors.CalcNumAtomStereoCenters(romol))






