## generic modules
import time
import multiprocessing
import pandas as pd
import sys
import gzip



#RDkit specific modules
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.Chem import PandasTools
from rdkit.Chem import AllChem
from rdkit.Chem import rdMolDescriptors

#MolVS specific modules

import molvs
from molvs import Standardizer
from molvs import Validator
# from molvs.fragment import LargestFragmentChooser
# from molvs.fragment import FragmentRemover
# or eventually load the local (modified) fragment.py
import fragment
from fragment import LargestFragmentChooser
from fragment import FragmentRemover
from molvs.charge import Uncharger


def printer(inchi):
    print(inchi)

def MolFromInchi_fun(inchi):
    m = Chem.MolFromInchi(inchi)
    if m:
        return m
    return None

# def MolFromInchi_fun(inchi):
#     m = Chem.MolFromInchi(inchi)
#     if m:
#         return m
#     print(inchi)

def MolToSmiles_fun(romol):
    m = Chem.MolToSmiles(romol)
    if m:
        return m
    return None

def MolFromSmiles_fun(smiles):
    m = Chem.MolFromSmiles(smiles)
    if m:
        return m
    return None

def MolToInchi_fun(romol):
    m = Chem.MolToInchi(romol)
    if m:
        return m
    return None

def MolToInchi_fun_safe(smiles, romol):
    #print(smiles) (beware as too much print statement will crash \\
    # interactve python console such as the one in vscode)
    if '[O]' not in smiles:
        m = Chem.MolToInchi(romol)
        if m:
            return m
        return None
    else:
        print('Sayonara Robocop !')
        return None

def MolToIK_fun(romol):
    m = Chem.MolToInchiKey(romol)
    if m:
        return m
    return None

def MolToIK_fun_safe(smiles, romol):
    #print(smiles) (beware as too much print statement will crash \\
    # interactve python console such as the one in vscode)
    if '[O]' not in smiles:
        m = Chem.MolToInchiKey(romol)
        if m:
            return m
        return None
    else:
        print('Sayonara Babeee !')
        return None


def MolToMF_fun(romol):
    m = rdMolDescriptors.CalcMolFormula(romol)
    if m:
        return m
    return None

def MolToEmass_fun(romol):
    m = Descriptors.ExactMolWt(romol)
    if m:
        return m
    return None

def MolToLogP_fun(romol):
    m = Chem.Crippen.MolLogP(romol)
    if m:
        return m
    return None



# defining the validator log output format
fmt = '%(asctime)s - %(levelname)s - %(validation)s - %(message)s'

# save the Standardizer and LargestFragmentChooser classes as variables

def validator_fun(romol):
    print(romol.GetNumAtoms)
    m = Validator(log_format=fmt).validate(romol)
    if m:
        return m
    return None

def standardizor_fun(romol):
    print('standardizer ' + str(romol.GetNumAtoms))
    m = Standardizer().standardize(romol)
    if m:
        return m
    return None

def fragremover_fun(romol):
    print('fragremover ' + str(romol.GetNumAtoms))
    m = FragmentRemover().remove(romol)
    if m:
        return m
    return None

def uncharger_fun(romol):
    print('uncharger ' + str(romol.GetNumAtoms))
    m = Uncharger().uncharge(romol)
    if m:
        return m
    return None


def AS_fun(romol):
    m = Chem.AssignStereochemistry(romol, force=True, cleanIt=True)
    if m:
        return m
    return None

# this one "Returns the number of unspecified atomic stereocenters"

def UnspecStereoCount_fun(romol):
    try:
        m = rdMolDescriptors.CalcNumUnspecifiedAtomStereoCenters(romol)
        if m:
            return m
            print(m)
        return None
    except Exception as e:
        print('failed', flush=True)
        return e


    



# this one "Returns the total number of atomic stereocenters (specified and unspecified)"

def StereoCount_fun(romol):
    m = rdMolDescriptors.CalcNumAtomStereoCenters(romol)
    if m:
        return m
    return None


# import random
# import multiprocessing as mp
# import time
# import logging

# logger=mp.log_to_stderr(logging.DEBUG)

# def Process(x):
#     try:
#         logger.info(x)
#         time.sleep(random.random())
#         raise Exception('Exception: ' + x)
#     finally:
#         logger.info('Finally: ' + x)

# result=mp.Pool(3).map(Process, ['1','2','3'])

