# chemo_sanitizer

A set of script using RDKit and MolVS to proceed to the standardization , sanitization and different format conversion. Parallelized.

In this state it take only InChI as inputs and returns sanitized SMILES, InChI, InChIKeys, Short InChIkeys, MolecularFormula, ExactMass and XLogP.

## Requirements 

Install the conda environment by

`conda env create -f environment.yml`



## Usage

Go to the src folder 
Then add input and output file path as first and second argument, InChI column header as third argument and finally the number of cpus you want to use.

Example :
        
```
cd src
python chemosanitizer.py ~/translatedStructureRdkit.tsv ./test.tsv structureTranslated 6
```



### checking the parralelization issue of the stereocount function (Chem.rdMolDescriptors.CalcNumAtomStereoCenters) 

See the C++ implementation of the function here <https://github.com/rdkit/rdkit/commit/552a78ed3ab521263d33ce6eeadc374a8c48ae44#diff-3afb9f1ee6ba66d2b1dcc6deb5638d45aa9fb5505a44d13dc119137f68dc6bdfR475-R510>



(works OK - not parralelized)

`python src/scripts/stereocounter.py /Users/pma/Dropbox/Research_UNIGE/git_repos/chemo_sanitizer/data/unique_out.tsv.gz /Users/pma/Dropbox/Research_UNIGE/git_repos/chemo_sanitizer/data/unique_out_ss.tsv.gz smilesSanitized`

(does not break but doesnt works)

`python src/scripts/chemosanitizer.py /Users/pma/Dropbox/Research_UNIGE/git_repos/chemo_sanitizer/data/unique.tsv.gz /Users/pma/Dropbox/Research_UNIGE/git_repos/chemo_sanitizer/data/unique_out.tsv.gz structureTranslated 8`


